package com.company.ICriteria;

import com.company.Product;

import java.util.List;

public interface Criteria {
    public List<Product> criteria (List<Product> productList);
}
