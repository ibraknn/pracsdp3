package com.company.Criteria;

import com.company.Enums.Color;
import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Color.RED;
import static com.company.Enums.Type.PC;

public class RedCriteria implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> redProducts = new ArrayList<>();

        for(Product product : productList){
            if(product.getColor().equals(RED)){
                redProducts.add(product);
            }
        }
        return redProducts;
    }
}
