package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Color.BLUE;
import static com.company.Enums.Color.GREEN;

public class BlueCriteria implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> blueProducts = new ArrayList<>();

        for(Product product : productList){
            if(product.getColor().equals(BLUE)){
                blueProducts.add(product);
            }
        }
        return blueProducts;
    }
}