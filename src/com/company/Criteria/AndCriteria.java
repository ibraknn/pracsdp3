package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.List;

public class AndCriteria implements Criteria {
    private Criteria first;
    private Criteria second;

    public AndCriteria(Criteria first, Criteria second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> firstProduct = first.criteria(productList);
        return second.criteria(firstProduct);
    }
}
