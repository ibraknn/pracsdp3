package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.List;

public class OrCriteria implements Criteria {
    private Criteria first;
    private Criteria second;

    public OrCriteria(Criteria first, Criteria second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> firstProduct = first.criteria(productList);
        List<Product> secondProduct = second.criteria(productList);

        for(Product product : secondProduct){
            if(!firstProduct.contains(product)){
                firstProduct.add(product);
            }
        }
        return firstProduct;
    }
}