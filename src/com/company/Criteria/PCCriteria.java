package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Type.PC;

public class PCCriteria implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> pcProducts = new ArrayList<>();

        for(Product product : productList){
            if(product.getType().equals(PC)){
                pcProducts.add(product);
            }
        }
        return pcProducts;
    }
}