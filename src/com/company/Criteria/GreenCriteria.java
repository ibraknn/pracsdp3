package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Color.GREEN;
import static com.company.Enums.Color.RED;

public class GreenCriteria  implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> greenProducts = new ArrayList<>();

        for(Product product : productList){
            if(product.getColor().equals(GREEN)){
                greenProducts.add(product);
            }
        }
        return greenProducts;
    }
}
