package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Type.LAPTOP;

public class LaptopCriteria implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> laptopProducts = new ArrayList<>();

        for(Product product : productList){
            if(product.getType().equals(LAPTOP)){
                laptopProducts.add(product);
            }
        }
        return laptopProducts;
    }
}
