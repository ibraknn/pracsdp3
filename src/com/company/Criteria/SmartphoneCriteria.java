package com.company.Criteria;

import com.company.ICriteria.Criteria;
import com.company.Product;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Type.SMARTPHONE;

public class SmartphoneCriteria implements Criteria {
    @Override
    public List<Product> criteria(List<Product> productList) {
        List<Product> SmartphoneProduct = new ArrayList<>();

        for(Product product : productList){
            if(product.getType().equals(SMARTPHONE)){
                SmartphoneProduct.add(product);
            }
        }
        return SmartphoneProduct;
    }
}