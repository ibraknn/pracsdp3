package com.company;

import com.company.Enums.Color;
import com.company.Enums.Type;

public class Product {
    private String name;
    private double price;
    private int year;
    private Type type;
    private Color color;

    public Product(String name, double price, int year, Type type, Color color) {
        this.name = name;
        this.price = price;
        this.year = year;
        this.type = type;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return name + " " + price + " " + year + " " + type + " " + color;
    }
}
