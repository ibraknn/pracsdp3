package com.company;

import com.company.Criteria.*;
import com.company.Enums.Color;
import com.company.Enums.Type;
import com.company.ICriteria.Criteria;

import java.util.ArrayList;
import java.util.List;

import static com.company.Enums.Color.*;
import static com.company.Enums.Type.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        List<Product> productList = new ArrayList<>();


        productList.add(new Product("IPHONE",1200,2020, SMARTPHONE, RED));
        productList.add(new Product("MACBOOK",2200,2020, LAPTOP, GREEN));
        productList.add(new Product("IMAC",3500,2020, PC, BLUE));
        productList.add(new Product("HUAWEI",200,2020, SMARTPHONE, RED));
        productList.add(new Product("IMAC2",3500,2020, PC, RED));

        Criteria laptopCriteria = new LaptopCriteria();
        Criteria smartphoneCriteria = new SmartphoneCriteria();
        Criteria pcCriteria = new PCCriteria();
        Criteria greenCriteria = new GreenCriteria();
        Criteria blueCriteria = new BlueCriteria();
        Criteria redCriteria = new RedCriteria();

        Criteria redSmartphone = new AndCriteria(redCriteria,smartphoneCriteria);
        Criteria redOrPC = new OrCriteria(redCriteria,pcCriteria);

        System.out.println(redSmartphone.criteria(productList));
    }
}
